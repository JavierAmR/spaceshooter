﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Propeller : MonoBehaviour {

    public TrailRenderer blue01;
    public TrailRenderer blue02;
    public TrailRenderer red01;
    public TrailRenderer red02;

    
    //DESACTIVARLOS AL INICIO
    void Awake () {
        Stop ();
    }
    
    //FUNCIÓN DE ACTIVACIÓN DE EMISORES AZULES
    public void BlueFire(){
        blue01.emitting = true;
        blue02.emitting = true;
    }

    //FUNCIÓN DE ACTIVACIÓN DE EMISORES ROJOS
    public void RedFire(){
        red01.emitting = true;
        red02.emitting = true;
    }

    //FUNCIÓN DE DESACTIVACIÓN
    public void Stop(){
        blue01.emitting = false;
        blue02.emitting = false;
        red01.emitting = false;
        red02.emitting = false;
    }
}

