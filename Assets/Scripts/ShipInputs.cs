﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipInputs : MonoBehaviour
{
    public Vector2 axis;

    public PlayerBehaviour player;  
    

    void Update()
    {
      //LECTOR DE INPUTS
      axis.x = Input.GetAxis ("Horizontal");
      axis.y = Input.GetAxis ("Vertical");
      
      //ENVIO DE INPUTS DE DISPARO
      if (Input.GetButton("Fire1"))
      {
            //Debug.Log("Estoy disparando");
            player.Shoot();
      }

      //ENVIO DE INPUTS AL SCRIPT DE PLAYER
      player.SetAxis(axis);  
    }
}
