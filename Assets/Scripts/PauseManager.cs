﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseManager : MonoBehaviour
{
    public GameObject PauseCanvas;

    public void Continue()
    {
        Time.timeScale = 1f;
        PauseCanvas.SetActive(false);
    }
    public void EnterPause()
    {
        Time.timeScale = 0f;
        PauseCanvas.SetActive(true);
    }
    public void ExitGame()
    {
        SceneManager.LoadScene("MainMenu");
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (!PauseCanvas.activeSelf)
            {
                EnterPause();
            }
            else
            {
                Continue();
            }
            
        }
    }
}
