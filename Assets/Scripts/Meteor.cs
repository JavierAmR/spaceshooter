﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Meteor : MonoBehaviour
{
    private Vector2 speed;

    private Transform graphics;
    private int selectedGraphic;

    public ParticleSystem ps;

    public AudioSource audioSource;

    private void Awake()
    {
        //SELECTOR DE GRAFICO
        graphics = transform.GetChild(0);
        selectedGraphic = Random.Range(0, graphics.childCount);
        for (int i = 0; i < graphics.childCount; i++)
        {
            if (i == selectedGraphic)
            {
                graphics.GetChild(i).gameObject.SetActive(true);
            }
            else
            {
                graphics.GetChild(i).gameObject.SetActive(false);
            }
            
        }

        //ASIGNACION DE VALOR DE VELOCIDAD
        speed.x = Random.Range(-5, 0);
        speed.y = Random.Range(-5, 6);

    }

    private void Update()
    {
        //MOVIMIENTO
        transform.Translate(speed * Time.deltaTime);
        graphics.Rotate(0, 0, 100 * Time.deltaTime);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //CONDICION DE DESTRUCCION
        if (collision.gameObject.tag == "Finish")
        {
            Destroy(gameObject);
        }
        else if (collision.gameObject.tag == "Bullet")
        {
            StartCoroutine(DestroyMeteor());
        }
    }

    IEnumerator DestroyMeteor()
    {
        graphics.gameObject.SetActive(false);

        Destroy(GetComponent<BoxCollider2D>());

        //Lanzar particula
        ps.Play();

        //Lanzo sonido de explosion
        audioSource.Play();

        //Me espero 1 segundo
        yield return new WaitForSeconds(1.0f);

        //Me destruyo a mi mismo
        Destroy(this.gameObject);
    }
}

