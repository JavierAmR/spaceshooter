﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Background : MonoBehaviour {

    public Material mat;
    public float smooth = 1.0f;
    private float offsetX = 0.0f;

    private Vector2 textoffset;



    
    void Awake () {
        //BUSQUEDA Y ASIGNACIÓN DEL COMPONENTE MATERIAL
        mat = GetComponent<Renderer> ().material;
        textoffset = new Vector2 (offsetX,0);
    }
    
    
    void Update () {

        //CALCULO DE LA VELOCIDAD DEL MOVIMIENTO DEL OFFSET
        offsetX += (Time.deltaTime * (smooth  + 75/100));
        if(offsetX >= 100) offsetX -= 100;

        //MODIFICADOR DEL OFFSET DE LA TEXTURA DEL MATERIAL
        textoffset.x = offsetX;
        mat.SetTextureOffset("_MainTex", textoffset);
    }

}