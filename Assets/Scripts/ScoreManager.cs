﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
    public PlayerBehaviour Player;

    public int scoreInt;

    [SerializeField] Text score;
    [SerializeField] RectTransform LifesGraphics;

    void Start()
    {
        scoreInt = 0;
        score.text = scoreInt.ToString("000000");
        //score.text = scoreInt.ToString();
    }

    public void AddScore(int value)
    {
        scoreInt += value;
        score.text = scoreInt.ToString("000000");
    }

    public void UpdateLife()
    {
        if (Player.lives >= 0)
        {
            LifesGraphics.GetChild(Player.lives).gameObject.SetActive(false);
        }
        
    }
}
