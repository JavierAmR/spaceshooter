﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserBullet : MonoBehaviour
{
    public Vector2 direction;
    public float speed;
    
    void Update()
    {
        //INSTRUCCIÓN DE MOVIMIENTO
        transform.Translate(direction*speed * Time.deltaTime);
    }

    
    void OnTriggerEnter2D(Collider2D other)
    {
        //DETECTOR DE COLISIÓN DE ELIMINACIÓN
        if (other.gameObject.tag == "Finish")
        {
            Destroy(gameObject);
        }
    }
}
