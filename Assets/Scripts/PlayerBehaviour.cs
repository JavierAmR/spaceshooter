﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBehaviour : MonoBehaviour
{
    public float speed;
    private Vector2 axis;    
    public Vector2 limits;

    public ScoreManager SM;

    public float shootTime = 0;

    public Weapon weapon;    

    public Propeller prop;

    [SerializeField] GameObject graphics;
    [SerializeField] Collider2D collider;
    [SerializeField] ParticleSystem ps;
    [SerializeField] AudioSource audioSource;

    public int lives = 3;
    private bool iamDead = false;

    private void Update()
    {
         if(iamDead){
            return;
        }
        shootTime += Time.deltaTime;

        //MOVIMIENTO DE LA NAVE
        transform.Translate (axis * speed * Time.deltaTime);

        //LIMITADORES

        //-Limitador Y
        if (transform.position.y > limits.y)
        {
            transform.position = new Vector3 (transform.position.x, limits.y, transform.position.z);
        }
        else if (transform.position.y < -limits.y)
        {
            transform.position = new Vector3 (transform.position.x, -limits.y, transform.position.z);
        }

        //-Limitador X
        if (transform.position.x > limits.x)
        {
            transform.position = new Vector3 (limits.x, transform.position.y, transform.position.z);
        }
        else if (transform.position.x < -limits.x)
        {
            transform.position = new Vector3 (-limits.x, transform.position.y, transform.position.z);
        }

        //PROPELLER (LLAMADA A FUNCIONES)
        
        //-Propulsores azules
        if (axis.x > 0)
        {
            prop.BlueFire();
        }
        //-Propulsores rojos
        else if (axis.x < 0)
        {
            prop.RedFire();
        }
        //-En caso de ningún movimiento ordenar la desactivación de los propulsores
        else 
        {
            prop.Stop();
        }
    }

    //RECEPTOR DE INPUTS DE MOVIMIENTO
    public void SetAxis(Vector2 currentaxis)
    {
        axis = currentaxis;
    }

    public void SetAxis(float x, float y)
    {
        axis = new Vector2(x,y);
    }

    //FUNCIÓN DE DISPARO
    public void Shoot()
    {
        if (shootTime > weapon.GetCadencia())
        {
            shootTime = 0;
            weapon.Shoot();
        }

        //Debug.Log("Sigo disparando pero desde otra función");
    }

    /// <summary>
    /// OnTriggerEnter is called when the Collider other enters the trigger.
    /// </summary>
    /// <param name="other">The other Collider involved in this collision.</param>
    public void OnTriggerEnter2D(Collider2D other){
        if(other.tag == "Meteor") {
            StartCoroutine(DestroyShip());
        }
    }

    IEnumerator DestroyShip(){
        //Indico que estoy muerto
        iamDead=true;

        //Me quito una vida
        lives--;

        SM.UpdateLife();



        //Desactivo el grafico
        //graphics.SetActive(false);

        //Elimino el BoxCollider2D
        //collider.enabled = false;

        //Lanzo la partícula
        //ps.Play();

        //Lanzo sonido de explosion
        //audioSource.Play();

        //Desactivo el propeller
        //prop.gameObject.SetActive(false);

        //Me espero 1 segundo
        yield return new WaitForSeconds(1.0f);
        
        //Miro si tengo mas vidas
        if(lives>0){
            //Vuelvo a activar el jugador
            iamDead = false;
            graphics.SetActive(true);
            collider.enabled = true;
            //Activo el propeller
            prop.gameObject.SetActive(true);
        }
    }
}
